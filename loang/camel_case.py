'''This module is CamelCase generetor'''


def camel_case(user_input):
    """This is code section"""
    ans = 1
    word = user_input.strip()
    if word[0].isupper():
        ans = 0
    else:
        ans = 1
    for i in word:
        if i.isupper():
            ans += 1
    return ans
