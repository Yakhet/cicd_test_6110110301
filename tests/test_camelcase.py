'''This module is the test of CamelCase'''


import unittest
from loang import camel_case


class TestCamelcase(unittest.TestCase):
    """This is code section"""
    def test_saveChangesInTheEditor_shoud_be_5(self):
        """input saveChangesInTheEditor"""
        result = camel_case.camel_case("saveChangesInTheEditor")
        self.assertEqual(result, 5, "Answer should be 5")

    def test_Cat_shoud_be_1(self):
        """input Cat"""
        result = camel_case.camel_case("Cat")
        self.assertEqual(result, 1, "Answer should be 1")

    def test_dogCat_shoud_be_2(self):
        """input dogCat"""
        result = camel_case.camel_case("dogCat")
        self.assertEqual(result, 2, "Answer should be 2")

    def test_normal_shoud_be_1(self):
        """input normal"""
        result = camel_case.camel_case("normal")
        self.assertEqual(result, 1, "Answer should be 1")

    def test_manWomanBoyGirlKidTeen_shoud_be_6(self):
        """input manWomanBoyGirlKidTeen"""
        result = camel_case.camel_case("manWomanBoyGirlKidTeen")
        self.assertEqual(result, 6, "Answer should be 6")

    def test_footballBasketballVolleyball_shoud_be_3(self):
        """input footballBasketballVolleyball"""
        result = camel_case.camel_case("footballBasketballVolleyball")
        self.assertEqual(result, 3, "Answer should be 3")

    def test_glassWaterMilkCocacola_shoud_be_4(self):
        """input glassWaterMilkCocacola"""
        result = camel_case.camel_case("glassWaterMilkCocacola")
        self.assertEqual(result, 4, "Answer should be 4")
